package demo1

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@SpringBootConfiguration
class Test1 {
    @Test
    fun test() {
        println("Привет, $greetingStr!")
    }

    //@Value("\${app.greeting:'Kotlin'}")
    private val greetingStr: String = "Kotlin"
}