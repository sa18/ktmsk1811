package demo3

import demo3.util.logger
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@ContextConfiguration(classes = [TestsRunConfig::class])
class Test1 {

    private val log by logger()

    @Test
    fun test() {
        log.info("Привет, $greetingStr!")
    }

    @Value("\${app.greeting:'Kotlin'}")
    private lateinit var greetingStr: String
}