package demo3.util

import org.jooq.Converter
import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

class LongToInstantConverter : Converter<Int, Instant> {

    override fun from(dbv: Int?) = if (dbv != null && dbv != 0) Timestamp(1000L * dbv).toInstant() else null

    override fun to(uv: Instant?) = if (uv != null) Timestamp.valueOf(LocalDateTime.ofInstant(uv, ZoneId.systemDefault())).time.toInt() else 0

    override fun fromType() = Int::class.java

    override fun toType() = Instant::class.java
}

class LongToBooleanConverter : Converter<Int, Boolean> {

    override fun from(v: Int?) = if (v != null) v != 0 else null

    override fun to(v: Boolean?) = if (v != null) if (v) 1 else 0 else null

    override fun fromType() = Int::class.java

    override fun toType() = Boolean::class.java
}
