package demo3.service

import demo3.repo.ProjectRepo
import org.springframework.stereotype.Service

@Service
class ProjectService(private val projRepo: ProjectRepo) {

    fun listProjects() = projRepo.listProjectsAsMap()

}