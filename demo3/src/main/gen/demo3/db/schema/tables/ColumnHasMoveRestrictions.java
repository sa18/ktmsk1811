/*
 * This file is generated by jOOQ.
 */
package demo3.db.schema.tables;


import demo3.db.schema.DefaultSchema;
import demo3.db.schema.Indexes;
import demo3.db.schema.Keys;
import demo3.db.schema.tables.records.ColumnHasMoveRestrictionsRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ColumnHasMoveRestrictions extends TableImpl<ColumnHasMoveRestrictionsRecord> {

    private static final long serialVersionUID = 318527792;

    /**
     * The reference instance of <code>column_has_move_restrictions</code>
     */
    public static final ColumnHasMoveRestrictions TColumnHasMoveRestrictions = new ColumnHasMoveRestrictions();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ColumnHasMoveRestrictionsRecord> getRecordType() {
        return ColumnHasMoveRestrictionsRecord.class;
    }

    /**
     * The column <code>column_has_move_restrictions.restriction_id</code>.
     */
    public final TableField<ColumnHasMoveRestrictionsRecord, Integer> restrictionId = createField("restriction_id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>column_has_move_restrictions.project_id</code>.
     */
    public final TableField<ColumnHasMoveRestrictionsRecord, Integer> projectId = createField("project_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>column_has_move_restrictions.role_id</code>.
     */
    public final TableField<ColumnHasMoveRestrictionsRecord, Integer> roleId = createField("role_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>column_has_move_restrictions.src_column_id</code>.
     */
    public final TableField<ColumnHasMoveRestrictionsRecord, Integer> srcColumnId = createField("src_column_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>column_has_move_restrictions.dst_column_id</code>.
     */
    public final TableField<ColumnHasMoveRestrictionsRecord, Integer> dstColumnId = createField("dst_column_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>column_has_move_restrictions.only_assigned</code>.
     */
    public final TableField<ColumnHasMoveRestrictionsRecord, Integer> onlyAssigned = createField("only_assigned", org.jooq.impl.SQLDataType.INTEGER.defaultValue(org.jooq.impl.DSL.field("0", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * Create a <code>column_has_move_restrictions</code> table reference
     */
    public ColumnHasMoveRestrictions() {
        this(DSL.name("column_has_move_restrictions"), null);
    }

    /**
     * Create an aliased <code>column_has_move_restrictions</code> table reference
     */
    public ColumnHasMoveRestrictions(String alias) {
        this(DSL.name(alias), TColumnHasMoveRestrictions);
    }

    /**
     * Create an aliased <code>column_has_move_restrictions</code> table reference
     */
    public ColumnHasMoveRestrictions(Name alias) {
        this(alias, TColumnHasMoveRestrictions);
    }

    private ColumnHasMoveRestrictions(Name alias, Table<ColumnHasMoveRestrictionsRecord> aliased) {
        this(alias, aliased, null);
    }

    private ColumnHasMoveRestrictions(Name alias, Table<ColumnHasMoveRestrictionsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> ColumnHasMoveRestrictions(Table<O> child, ForeignKey<O, ColumnHasMoveRestrictionsRecord> key) {
        super(child, key, TColumnHasMoveRestrictions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.SQLITE_AUTOINDEX_COLUMN_HAS_MOVE_RESTRICTIONS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<ColumnHasMoveRestrictionsRecord> getPrimaryKey() {
        return Keys.PK_COLUMN_HAS_MOVE_RESTRICTIONS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<ColumnHasMoveRestrictionsRecord>> getKeys() {
        return Arrays.<UniqueKey<ColumnHasMoveRestrictionsRecord>>asList(Keys.PK_COLUMN_HAS_MOVE_RESTRICTIONS, Keys.SQLITE_AUTOINDEX_COLUMN_HAS_MOVE_RESTRICTIONS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<ColumnHasMoveRestrictionsRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<ColumnHasMoveRestrictionsRecord, ?>>asList(Keys.FK_COLUMN_HAS_MOVE_RESTRICTIONS_PROJECTS_1, Keys.FK_COLUMN_HAS_MOVE_RESTRICTIONS_PROJECT_HAS_ROLES_1, Keys.FK_COLUMN_HAS_MOVE_RESTRICTIONS_COLUMNS_2, Keys.FK_COLUMN_HAS_MOVE_RESTRICTIONS_COLUMNS_1);
    }

    public Projects projects() {
        return new Projects(this, Keys.FK_COLUMN_HAS_MOVE_RESTRICTIONS_PROJECTS_1);
    }

    public ProjectHasRoles projectHasRoles() {
        return new ProjectHasRoles(this, Keys.FK_COLUMN_HAS_MOVE_RESTRICTIONS_PROJECT_HAS_ROLES_1);
    }

    public Columns fkColumnHasMoveRestrictionsColumns_2() {
        return new Columns(this, Keys.FK_COLUMN_HAS_MOVE_RESTRICTIONS_COLUMNS_2);
    }

    public Columns fkColumnHasMoveRestrictionsColumns_1() {
        return new Columns(this, Keys.FK_COLUMN_HAS_MOVE_RESTRICTIONS_COLUMNS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ColumnHasMoveRestrictions as(String alias) {
        return new ColumnHasMoveRestrictions(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ColumnHasMoveRestrictions as(Name alias) {
        return new ColumnHasMoveRestrictions(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public ColumnHasMoveRestrictions rename(String name) {
        return new ColumnHasMoveRestrictions(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public ColumnHasMoveRestrictions rename(Name name) {
        return new ColumnHasMoveRestrictions(name, null);
    }
}
