/*
 * This file is generated by jOOQ.
 */
package demo3.db.schema.tables;


import demo3.db.schema.DefaultSchema;
import demo3.db.schema.Indexes;
import demo3.db.schema.Keys;
import demo3.db.schema.tables.records.UserHasNotificationsRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserHasNotifications extends TableImpl<UserHasNotificationsRecord> {

    private static final long serialVersionUID = 610316955;

    /**
     * The reference instance of <code>user_has_notifications</code>
     */
    public static final UserHasNotifications TUserHasNotifications = new UserHasNotifications();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<UserHasNotificationsRecord> getRecordType() {
        return UserHasNotificationsRecord.class;
    }

    /**
     * The column <code>user_has_notifications.user_id</code>.
     */
    public final TableField<UserHasNotificationsRecord, Integer> userId = createField("user_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>user_has_notifications.project_id</code>.
     */
    public final TableField<UserHasNotificationsRecord, Integer> projectId = createField("project_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>user_has_notifications</code> table reference
     */
    public UserHasNotifications() {
        this(DSL.name("user_has_notifications"), null);
    }

    /**
     * Create an aliased <code>user_has_notifications</code> table reference
     */
    public UserHasNotifications(String alias) {
        this(DSL.name(alias), TUserHasNotifications);
    }

    /**
     * Create an aliased <code>user_has_notifications</code> table reference
     */
    public UserHasNotifications(Name alias) {
        this(alias, TUserHasNotifications);
    }

    private UserHasNotifications(Name alias, Table<UserHasNotificationsRecord> aliased) {
        this(alias, aliased, null);
    }

    private UserHasNotifications(Name alias, Table<UserHasNotificationsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> UserHasNotifications(Table<O> child, ForeignKey<O, UserHasNotificationsRecord> key) {
        super(child, key, TUserHasNotifications);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.SQLITE_AUTOINDEX_USER_HAS_NOTIFICATIONS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<UserHasNotificationsRecord>> getKeys() {
        return Arrays.<UniqueKey<UserHasNotificationsRecord>>asList(Keys.SQLITE_AUTOINDEX_USER_HAS_NOTIFICATIONS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<UserHasNotificationsRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<UserHasNotificationsRecord, ?>>asList(Keys.FK_USER_HAS_NOTIFICATIONS_USERS_1, Keys.FK_USER_HAS_NOTIFICATIONS_PROJECTS_1);
    }

    public Users users() {
        return new Users(this, Keys.FK_USER_HAS_NOTIFICATIONS_USERS_1);
    }

    public Projects projects() {
        return new Projects(this, Keys.FK_USER_HAS_NOTIFICATIONS_PROJECTS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserHasNotifications as(String alias) {
        return new UserHasNotifications(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserHasNotifications as(Name alias) {
        return new UserHasNotifications(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public UserHasNotifications rename(String name) {
        return new UserHasNotifications(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public UserHasNotifications rename(Name name) {
        return new UserHasNotifications(name, null);
    }
}
