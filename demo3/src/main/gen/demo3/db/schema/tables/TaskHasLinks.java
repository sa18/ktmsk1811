/*
 * This file is generated by jOOQ.
 */
package demo3.db.schema.tables;


import demo3.db.schema.DefaultSchema;
import demo3.db.schema.Indexes;
import demo3.db.schema.Keys;
import demo3.db.schema.tables.records.TaskHasLinksRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TaskHasLinks extends TableImpl<TaskHasLinksRecord> {

    private static final long serialVersionUID = 1944246573;

    /**
     * The reference instance of <code>task_has_links</code>
     */
    public static final TaskHasLinks TTaskHasLinks = new TaskHasLinks();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TaskHasLinksRecord> getRecordType() {
        return TaskHasLinksRecord.class;
    }

    /**
     * The column <code>task_has_links.id</code>.
     */
    public final TableField<TaskHasLinksRecord, Integer> id = createField("id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>task_has_links.link_id</code>.
     */
    public final TableField<TaskHasLinksRecord, Integer> linkId = createField("link_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>task_has_links.task_id</code>.
     */
    public final TableField<TaskHasLinksRecord, Integer> taskId = createField("task_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>task_has_links.opposite_task_id</code>.
     */
    public final TableField<TaskHasLinksRecord, Integer> oppositeTaskId = createField("opposite_task_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>task_has_links</code> table reference
     */
    public TaskHasLinks() {
        this(DSL.name("task_has_links"), null);
    }

    /**
     * Create an aliased <code>task_has_links</code> table reference
     */
    public TaskHasLinks(String alias) {
        this(DSL.name(alias), TTaskHasLinks);
    }

    /**
     * Create an aliased <code>task_has_links</code> table reference
     */
    public TaskHasLinks(Name alias) {
        this(alias, TTaskHasLinks);
    }

    private TaskHasLinks(Name alias, Table<TaskHasLinksRecord> aliased) {
        this(alias, aliased, null);
    }

    private TaskHasLinks(Name alias, Table<TaskHasLinksRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> TaskHasLinks(Table<O> child, ForeignKey<O, TaskHasLinksRecord> key) {
        super(child, key, TTaskHasLinks);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.TASK_HAS_LINKS_TASK_INDEX, Indexes.TASK_HAS_LINKS_UNIQUE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TaskHasLinksRecord> getPrimaryKey() {
        return Keys.PK_TASK_HAS_LINKS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TaskHasLinksRecord>> getKeys() {
        return Arrays.<UniqueKey<TaskHasLinksRecord>>asList(Keys.PK_TASK_HAS_LINKS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<TaskHasLinksRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<TaskHasLinksRecord, ?>>asList(Keys.FK_TASK_HAS_LINKS_LINKS_1, Keys.FK_TASK_HAS_LINKS_TASKS_2, Keys.FK_TASK_HAS_LINKS_TASKS_1);
    }

    public Links links() {
        return new Links(this, Keys.FK_TASK_HAS_LINKS_LINKS_1);
    }

    public Tasks fkTaskHasLinksTasks_2() {
        return new Tasks(this, Keys.FK_TASK_HAS_LINKS_TASKS_2);
    }

    public Tasks fkTaskHasLinksTasks_1() {
        return new Tasks(this, Keys.FK_TASK_HAS_LINKS_TASKS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasLinks as(String alias) {
        return new TaskHasLinks(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasLinks as(Name alias) {
        return new TaskHasLinks(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TaskHasLinks rename(String name) {
        return new TaskHasLinks(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TaskHasLinks rename(Name name) {
        return new TaskHasLinks(name, null);
    }
}
