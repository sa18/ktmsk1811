/*
 * This file is generated by jOOQ.
 */
package org.example.ktmsk201811.db.schema.tables.records;


import java.time.Instant;

import javax.annotation.Generated;

import org.example.ktmsk201811.db.schema.tables.TaskHasExternalLinks;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record9;
import org.jooq.Row9;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TaskHasExternalLinksRecord extends UpdatableRecordImpl<TaskHasExternalLinksRecord> implements Record9<Integer, String, String, String, String, Instant, Instant, Integer, Integer> {

    private static final long serialVersionUID = -2141244823;

    /**
     * Setter for <code>task_has_external_links.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>task_has_external_links.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>task_has_external_links.link_type</code>.
     */
    public void setLinkType(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>task_has_external_links.link_type</code>.
     */
    public String getLinkType() {
        return (String) get(1);
    }

    /**
     * Setter for <code>task_has_external_links.dependency</code>.
     */
    public void setDependency(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>task_has_external_links.dependency</code>.
     */
    public String getDependency() {
        return (String) get(2);
    }

    /**
     * Setter for <code>task_has_external_links.title</code>.
     */
    public void setTitle(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>task_has_external_links.title</code>.
     */
    public String getTitle() {
        return (String) get(3);
    }

    /**
     * Setter for <code>task_has_external_links.url</code>.
     */
    public void setUrl(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>task_has_external_links.url</code>.
     */
    public String getUrl() {
        return (String) get(4);
    }

    /**
     * Setter for <code>task_has_external_links.date_creation</code>.
     */
    public void setDateCreation(Instant value) {
        set(5, value);
    }

    /**
     * Getter for <code>task_has_external_links.date_creation</code>.
     */
    public Instant getDateCreation() {
        return (Instant) get(5);
    }

    /**
     * Setter for <code>task_has_external_links.date_modification</code>.
     */
    public void setDateModification(Instant value) {
        set(6, value);
    }

    /**
     * Getter for <code>task_has_external_links.date_modification</code>.
     */
    public Instant getDateModification() {
        return (Instant) get(6);
    }

    /**
     * Setter for <code>task_has_external_links.task_id</code>.
     */
    public void setTaskId(Integer value) {
        set(7, value);
    }

    /**
     * Getter for <code>task_has_external_links.task_id</code>.
     */
    public Integer getTaskId() {
        return (Integer) get(7);
    }

    /**
     * Setter for <code>task_has_external_links.creator_id</code>.
     */
    public void setCreatorId(Integer value) {
        set(8, value);
    }

    /**
     * Getter for <code>task_has_external_links.creator_id</code>.
     */
    public Integer getCreatorId() {
        return (Integer) get(8);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record9 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row9<Integer, String, String, String, String, Instant, Instant, Integer, Integer> fieldsRow() {
        return (Row9) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row9<Integer, String, String, String, String, Instant, Instant, Integer, Integer> valuesRow() {
        return (Row9) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.linkType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.dependency;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.title;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.url;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Instant> field6() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.dateCreation;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Instant> field7() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.dateModification;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field8() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.taskId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field9() {
        return TaskHasExternalLinks.TTaskHasExternalLinks.creatorId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getLinkType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getDependency();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getTitle();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component5() {
        return getUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Instant component6() {
        return getDateCreation();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Instant component7() {
        return getDateModification();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component8() {
        return getTaskId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component9() {
        return getCreatorId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getLinkType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getDependency();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getTitle();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Instant value6() {
        return getDateCreation();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Instant value7() {
        return getDateModification();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value8() {
        return getTaskId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value9() {
        return getCreatorId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value2(String value) {
        setLinkType(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value3(String value) {
        setDependency(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value4(String value) {
        setTitle(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value5(String value) {
        setUrl(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value6(Instant value) {
        setDateCreation(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value7(Instant value) {
        setDateModification(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value8(Integer value) {
        setTaskId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord value9(Integer value) {
        setCreatorId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskHasExternalLinksRecord values(Integer value1, String value2, String value3, String value4, String value5, Instant value6, Instant value7, Integer value8, Integer value9) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TaskHasExternalLinksRecord
     */
    public TaskHasExternalLinksRecord() {
        super(TaskHasExternalLinks.TTaskHasExternalLinks);
    }

    /**
     * Create a detached, initialised TaskHasExternalLinksRecord
     */
    public TaskHasExternalLinksRecord(Integer id, String linkType, String dependency, String title, String url, Instant dateCreation, Instant dateModification, Integer taskId, Integer creatorId) {
        super(TaskHasExternalLinks.TTaskHasExternalLinks);

        set(0, id);
        set(1, linkType);
        set(2, dependency);
        set(3, title);
        set(4, url);
        set(5, dateCreation);
        set(6, dateModification);
        set(7, taskId);
        set(8, creatorId);
    }
}
