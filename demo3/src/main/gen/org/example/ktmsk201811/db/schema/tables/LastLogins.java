/*
 * This file is generated by jOOQ.
 */
package org.example.ktmsk201811.db.schema.tables;


import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.example.ktmsk201811.db.schema.DefaultSchema;
import org.example.ktmsk201811.db.schema.Indexes;
import org.example.ktmsk201811.db.schema.Keys;
import org.example.ktmsk201811.db.schema.tables.records.LastLoginsRecord;
import org.example.ktmsk201811.demo3.util.LongToInstantConverter;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class LastLogins extends TableImpl<LastLoginsRecord> {

    private static final long serialVersionUID = -1863923525;

    /**
     * The reference instance of <code>last_logins</code>
     */
    public static final LastLogins TLastLogins = new LastLogins();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<LastLoginsRecord> getRecordType() {
        return LastLoginsRecord.class;
    }

    /**
     * The column <code>last_logins.id</code>.
     */
    public final TableField<LastLoginsRecord, Integer> id = createField("id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>last_logins.auth_type</code>.
     */
    public final TableField<LastLoginsRecord, String> authType = createField("auth_type", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>last_logins.user_id</code>.
     */
    public final TableField<LastLoginsRecord, Integer> userId = createField("user_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>last_logins.ip</code>.
     */
    public final TableField<LastLoginsRecord, String> ip = createField("ip", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>last_logins.user_agent</code>.
     */
    public final TableField<LastLoginsRecord, String> userAgent = createField("user_agent", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>last_logins.date_creation</code>.
     */
    public final TableField<LastLoginsRecord, Instant> dateCreation = createField("date_creation", org.jooq.impl.SQLDataType.INTEGER, this, "", new LongToInstantConverter());

    /**
     * Create a <code>last_logins</code> table reference
     */
    public LastLogins() {
        this(DSL.name("last_logins"), null);
    }

    /**
     * Create an aliased <code>last_logins</code> table reference
     */
    public LastLogins(String alias) {
        this(DSL.name(alias), TLastLogins);
    }

    /**
     * Create an aliased <code>last_logins</code> table reference
     */
    public LastLogins(Name alias) {
        this(alias, TLastLogins);
    }

    private LastLogins(Name alias, Table<LastLoginsRecord> aliased) {
        this(alias, aliased, null);
    }

    private LastLogins(Name alias, Table<LastLoginsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> LastLogins(Table<O> child, ForeignKey<O, LastLoginsRecord> key) {
        super(child, key, TLastLogins);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.LAST_LOGINS_USER_IDX);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<LastLoginsRecord> getPrimaryKey() {
        return Keys.PK_LAST_LOGINS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<LastLoginsRecord>> getKeys() {
        return Arrays.<UniqueKey<LastLoginsRecord>>asList(Keys.PK_LAST_LOGINS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<LastLoginsRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<LastLoginsRecord, ?>>asList(Keys.FK_LAST_LOGINS_USERS_1);
    }

    public Users users() {
        return new Users(this, Keys.FK_LAST_LOGINS_USERS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LastLogins as(String alias) {
        return new LastLogins(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LastLogins as(Name alias) {
        return new LastLogins(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public LastLogins rename(String name) {
        return new LastLogins(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public LastLogins rename(Name name) {
        return new LastLogins(name, null);
    }
}
