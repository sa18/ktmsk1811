/*
 * This file is generated by jOOQ.
 */
package org.example.ktmsk201811.db.schema.tables;


import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.example.ktmsk201811.db.schema.DefaultSchema;
import org.example.ktmsk201811.db.schema.Keys;
import org.example.ktmsk201811.db.schema.tables.records.UserHasUnreadNotificationsRecord;
import org.example.ktmsk201811.demo3.util.LongToInstantConverter;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserHasUnreadNotifications extends TableImpl<UserHasUnreadNotificationsRecord> {

    private static final long serialVersionUID = 298658249;

    /**
     * The reference instance of <code>user_has_unread_notifications</code>
     */
    public static final UserHasUnreadNotifications TUserHasUnreadNotifications = new UserHasUnreadNotifications();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<UserHasUnreadNotificationsRecord> getRecordType() {
        return UserHasUnreadNotificationsRecord.class;
    }

    /**
     * The column <code>user_has_unread_notifications.id</code>.
     */
    public final TableField<UserHasUnreadNotificationsRecord, Integer> id = createField("id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>user_has_unread_notifications.user_id</code>.
     */
    public final TableField<UserHasUnreadNotificationsRecord, Integer> userId = createField("user_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>user_has_unread_notifications.date_creation</code>.
     */
    public final TableField<UserHasUnreadNotificationsRecord, Instant> dateCreation = createField("date_creation", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "", new LongToInstantConverter());

    /**
     * The column <code>user_has_unread_notifications.event_name</code>.
     */
    public final TableField<UserHasUnreadNotificationsRecord, String> eventName = createField("event_name", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>user_has_unread_notifications.event_data</code>.
     */
    public final TableField<UserHasUnreadNotificationsRecord, String> eventData = createField("event_data", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * Create a <code>user_has_unread_notifications</code> table reference
     */
    public UserHasUnreadNotifications() {
        this(DSL.name("user_has_unread_notifications"), null);
    }

    /**
     * Create an aliased <code>user_has_unread_notifications</code> table reference
     */
    public UserHasUnreadNotifications(String alias) {
        this(DSL.name(alias), TUserHasUnreadNotifications);
    }

    /**
     * Create an aliased <code>user_has_unread_notifications</code> table reference
     */
    public UserHasUnreadNotifications(Name alias) {
        this(alias, TUserHasUnreadNotifications);
    }

    private UserHasUnreadNotifications(Name alias, Table<UserHasUnreadNotificationsRecord> aliased) {
        this(alias, aliased, null);
    }

    private UserHasUnreadNotifications(Name alias, Table<UserHasUnreadNotificationsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> UserHasUnreadNotifications(Table<O> child, ForeignKey<O, UserHasUnreadNotificationsRecord> key) {
        super(child, key, TUserHasUnreadNotifications);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<UserHasUnreadNotificationsRecord> getPrimaryKey() {
        return Keys.PK_USER_HAS_UNREAD_NOTIFICATIONS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<UserHasUnreadNotificationsRecord>> getKeys() {
        return Arrays.<UniqueKey<UserHasUnreadNotificationsRecord>>asList(Keys.PK_USER_HAS_UNREAD_NOTIFICATIONS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<UserHasUnreadNotificationsRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<UserHasUnreadNotificationsRecord, ?>>asList(Keys.FK_USER_HAS_UNREAD_NOTIFICATIONS_USERS_1);
    }

    public Users users() {
        return new Users(this, Keys.FK_USER_HAS_UNREAD_NOTIFICATIONS_USERS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserHasUnreadNotifications as(String alias) {
        return new UserHasUnreadNotifications(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserHasUnreadNotifications as(Name alias) {
        return new UserHasUnreadNotifications(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public UserHasUnreadNotifications rename(String name) {
        return new UserHasUnreadNotifications(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public UserHasUnreadNotifications rename(Name name) {
        return new UserHasUnreadNotifications(name, null);
    }
}
