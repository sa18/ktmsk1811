/*
 * This file is generated by jOOQ.
 */
package org.example.ktmsk201811.db.schema.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.example.ktmsk201811.db.schema.DefaultSchema;
import org.example.ktmsk201811.db.schema.Indexes;
import org.example.ktmsk201811.db.schema.Keys;
import org.example.ktmsk201811.db.schema.tables.records.LinksRecord;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Links extends TableImpl<LinksRecord> {

    private static final long serialVersionUID = -281127070;

    /**
     * The reference instance of <code>links</code>
     */
    public static final Links TLinks = new Links();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<LinksRecord> getRecordType() {
        return LinksRecord.class;
    }

    /**
     * The column <code>links.id</code>.
     */
    public final TableField<LinksRecord, Integer> id = createField("id", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>links.label</code>.
     */
    public final TableField<LinksRecord, String> label = createField("label", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>links.opposite_id</code>.
     */
    public final TableField<LinksRecord, Integer> oppositeId = createField("opposite_id", org.jooq.impl.SQLDataType.INTEGER.defaultValue(org.jooq.impl.DSL.field("0", org.jooq.impl.SQLDataType.INTEGER)), this, "");

    /**
     * Create a <code>links</code> table reference
     */
    public Links() {
        this(DSL.name("links"), null);
    }

    /**
     * Create an aliased <code>links</code> table reference
     */
    public Links(String alias) {
        this(DSL.name(alias), TLinks);
    }

    /**
     * Create an aliased <code>links</code> table reference
     */
    public Links(Name alias) {
        this(alias, TLinks);
    }

    private Links(Name alias, Table<LinksRecord> aliased) {
        this(alias, aliased, null);
    }

    private Links(Name alias, Table<LinksRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Links(Table<O> child, ForeignKey<O, LinksRecord> key) {
        super(child, key, TLinks);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.SQLITE_AUTOINDEX_LINKS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<LinksRecord> getPrimaryKey() {
        return Keys.PK_LINKS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<LinksRecord>> getKeys() {
        return Arrays.<UniqueKey<LinksRecord>>asList(Keys.PK_LINKS, Keys.SQLITE_AUTOINDEX_LINKS_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Links as(String alias) {
        return new Links(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Links as(Name alias) {
        return new Links(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Links rename(String name) {
        return new Links(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Links rename(Name name) {
        return new Links(name, null);
    }
}
