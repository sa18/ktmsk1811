/*
 * This file is generated by jOOQ.
 */
package org.example.ktmsk201811.db.schema.tables.records;


import javax.annotation.Generated;

import org.example.ktmsk201811.db.schema.tables.ProjectHasMetadata;
import org.jooq.Field;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.5"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProjectHasMetadataRecord extends TableRecordImpl<ProjectHasMetadataRecord> implements Record5<Integer, String, String, Integer, Integer> {

    private static final long serialVersionUID = -1530709953;

    /**
     * Setter for <code>project_has_metadata.project_id</code>.
     */
    public void setProjectId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>project_has_metadata.project_id</code>.
     */
    public Integer getProjectId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>project_has_metadata.name</code>.
     */
    public void setName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>project_has_metadata.name</code>.
     */
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>project_has_metadata.value</code>.
     */
    public void setValue(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>project_has_metadata.value</code>.
     */
    public String getValue() {
        return (String) get(2);
    }

    /**
     * Setter for <code>project_has_metadata.changed_by</code>.
     */
    public void setChangedBy(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>project_has_metadata.changed_by</code>.
     */
    public Integer getChangedBy() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>project_has_metadata.changed_on</code>.
     */
    public void setChangedOn(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>project_has_metadata.changed_on</code>.
     */
    public Integer getChangedOn() {
        return (Integer) get(4);
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, String, String, Integer, Integer> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, String, String, Integer, Integer> valuesRow() {
        return (Row5) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return ProjectHasMetadata.TProjectHasMetadata.projectId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return ProjectHasMetadata.TProjectHasMetadata.name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return ProjectHasMetadata.TProjectHasMetadata.value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field4() {
        return ProjectHasMetadata.TProjectHasMetadata.changedBy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field5() {
        return ProjectHasMetadata.TProjectHasMetadata.changedOn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getProjectId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component4() {
        return getChangedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component5() {
        return getChangedOn();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getProjectId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value4() {
        return getChangedBy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value5() {
        return getChangedOn();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectHasMetadataRecord value1(Integer value) {
        setProjectId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectHasMetadataRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectHasMetadataRecord value3(String value) {
        setValue(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectHasMetadataRecord value4(Integer value) {
        setChangedBy(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectHasMetadataRecord value5(Integer value) {
        setChangedOn(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProjectHasMetadataRecord values(Integer value1, String value2, String value3, Integer value4, Integer value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ProjectHasMetadataRecord
     */
    public ProjectHasMetadataRecord() {
        super(ProjectHasMetadata.TProjectHasMetadata);
    }

    /**
     * Create a detached, initialised ProjectHasMetadataRecord
     */
    public ProjectHasMetadataRecord(Integer projectId, String name, String value, Integer changedBy, Integer changedOn) {
        super(ProjectHasMetadata.TProjectHasMetadata);

        set(0, projectId);
        set(1, name);
        set(2, value);
        set(3, changedBy);
        set(4, changedOn);
    }
}
