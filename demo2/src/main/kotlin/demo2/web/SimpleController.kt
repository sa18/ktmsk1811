package demo2.web

import demo2.domain.SampleResponse
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime

@RestController
class SimpleController {

    @GetMapping("/test1")
    fun test1() = "Привет!"

    @GetMapping("/test2")
    fun test2(): SampleResponse {
        return SampleResponse("запрос обработан",
                LocalDateTime.now(),
                IntRange(2, 200),
                arrayListOf(
                        Triple(100.toBigDecimal(), "A", 0.001),
                        Triple(200.toBigDecimal(), "B", 0.002),
                        Triple(300.toBigDecimal(), "C", 0.003)))
    }

}