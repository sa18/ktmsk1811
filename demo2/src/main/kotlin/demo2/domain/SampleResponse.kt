package demo2.domain

import java.math.BigDecimal
import java.time.LocalDateTime

data class SampleResponse(
        val message: String,
        val resonseTime: LocalDateTime,
        val range: IntRange,
        val someExoticData: Collection<Triple<BigDecimal, String, Double>>,
        val error: Throwable? = null
)
