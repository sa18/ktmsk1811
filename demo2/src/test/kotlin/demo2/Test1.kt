package demo2

import demo2.util.logger
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@SpringBootConfiguration
class Test1 {

    private val log = LoggerFactory.getLogger(Test1::class.java)
    //private val log by logger()

    @Test
    fun test() {
        log.info("Привет, $greetingStr!")
    }

    @Value("\${app.greeting:'Kotlin'}")
    private lateinit var greetingStr: String
}